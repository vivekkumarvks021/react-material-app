import { Box } from "@mui/system";

export const MuiResponsiveness = () => {
  return (
    <Box
      sx={{
        height: "300px",
        width: {
          xs: 100, //0
          sm: 200, //600
          md: 300, //900
          lg: 400, //1200
          xl: 500, //1536
        },
        bgcolor: "secondary.dark",
      }}
    ></Box>
  );
};
